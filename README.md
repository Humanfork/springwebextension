Humanfork: Spring Web Extension
-------------------------------

A util project that contains different small extensions for Spring Web applications.
The main extensions are:

- Exception Resolver that put validation errors in the HTTP-response body (package `de.humanfork.spring.web.exception.json`)
- Spring Security web AuthenticationEntryPoint that is able to switch between different AuthenticationEntryPoints, depending on the HTTP Request (package `de.humanfork.spring.web.security.authentication`)
- Different Servlet Tags (package `de.humanfork.spring.web.servlet.tags`)

Maven
-----
Available from [Maven Central](https://search.maven.org/search?q=g:de.humanfork.springwebextension)

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.humanfork.springwebextension/springwebextension/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.humanfork.springwebextension/springwebextension)

```
<dependency>
    <groupId>de.humanfork.springwebextension</groupId>
    <artifactId>springwebextension</artifactId>
    <version>0.7.7</version> <!-- or newer -->
</dependency>
```

Website
-------
- https://www.humanfork.de/projects/springwebextension/ (for releases)
- https://humanfork.gitlab.io/springwebextension/index.html (for snapshots)
