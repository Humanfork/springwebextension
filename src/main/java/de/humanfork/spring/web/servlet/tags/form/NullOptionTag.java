/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.servlet.tags.form;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;

import org.springframework.web.servlet.support.BindStatus;
import org.springframework.web.servlet.tags.form.OptionTag;
import org.springframework.web.servlet.tags.form.TagWriter;

/**
 * HTML Option tag that can be uses to represent the empty/not selected option in a option list.
 *
 * <pre>
 * &lt;form:select id=&quot;_${sec_field}_id&quot;  disabled=&quot;${disabled}&quot;&gt;
 *    &lt;c:if test=&quot;${not required}&quot;&gt;
 *	  &lt;formext:nulloption value=&quot;null&quot;&gt;&lt;/formext:nulloption&gt;
 *    &lt;/c:if&gt;
 *    &lt;form:options items=&quot;${items}&quot; path=&quot;${sec_field}&quot;
 *     itemValue=&quot;${fn:escapeXml(itemValue)}&quot; /&gt;
 * &lt;/form:select&gt;
 * </pre>
 *
 * @author Ralph Engelmann
 *
 */
public class NullOptionTag extends OptionTag {

    /** Serial version UIF. */
    private static final long serialVersionUID = 4845174006759536746L;

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.tags.form.OptionTag#renderDefaultContent(org.springframework.web.servlet.tags.form.TagWriter)
     */
    @Override
    protected void renderDefaultContent(final TagWriter tagWriter) throws JspException {
        Object value = this.pageContext.getAttribute(VALUE_VARIABLE_NAME);
        renderOptionSpecialWay(value, "", tagWriter);
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.tags.form.OptionTag#renderFromBodyContent(javax.servlet.jsp.tagext.BodyContent, org.springframework.web.servlet.tags.form.TagWriter)
     */
    @Override
    protected void renderFromBodyContent(final BodyContent bodyContent, final TagWriter tagWriter) throws JspException {
        Object value = this.pageContext.getAttribute(VALUE_VARIABLE_NAME);
        String label = bodyContent.getString();
        renderOptionSpecialWay(value, label, tagWriter);
    }

    /**
     * Render option by using the {@link #isSelected(Object)} method of this class.
     *
     * @param value the value
     * @param label the label
     * @param tagWriter the tag writer
     * @throws JspException the jsp exception
     */
    private void renderOptionSpecialWay(final Object value, final String label, final TagWriter tagWriter)
            throws JspException {
        tagWriter.startTag("option");
        writeOptionalAttribute(tagWriter, "id", resolveId());
        writeOptionalAttributes(tagWriter);
        String renderedValue = getDisplayString(value, getBindStatus().getEditor());
        tagWriter.writeAttribute(OptionTag.VALUE_VARIABLE_NAME, renderedValue);
        if (isSelected(value)) {
            tagWriter.writeAttribute("selected", "selected");
        }
        if (isDisabled()) {
            tagWriter.writeAttribute("disabled", "disabled");
        }
        tagWriter.appendValue(label);
        tagWriter.endTag();
    }

    /**
     * Checks if is selected.
     *
     * @param resolvedValue the resolved value
     * @return true, if is selected
     */
    private boolean isSelected(final Object resolvedValue) {
        BindStatus bindStatus = getBindStatus();

        return (bindStatus == null) || (bindStatus.getValue() == null) || (bindStatus.getActualValue() == null);
    }
}
