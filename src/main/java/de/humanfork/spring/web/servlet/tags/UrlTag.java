/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.servlet.tags;

import javax.servlet.jsp.JspException;

import org.springframework.web.context.WebApplicationContext;

/** Modify the url a bit. */
public class UrlTag extends org.springframework.web.servlet.tags.UrlTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 976132487491614481L;

    /** The Constant RESOURCES_FOLDER. */
    private static final String RESOURCES_FOLDER = "resources";

    /** The captured value. */
    private String capturedValue = null;

    @Override
    public int doEndTag() throws JspException {
        if (this.capturedValue != null) {
            if (this.capturedValue.startsWith(RESOURCES_FOLDER + "/")) {
                super.setValue(RESOURCES_FOLDER + getResourceVersion()
                        + this.capturedValue.substring(RESOURCES_FOLDER.length()));
            } else if (this.capturedValue.startsWith("/" + RESOURCES_FOLDER + "/")) {
                super.setValue("/" + RESOURCES_FOLDER + getResourceVersion()
                        + this.capturedValue.substring(("/" + RESOURCES_FOLDER).length()));
            } else if (this.capturedValue.startsWith("../" + RESOURCES_FOLDER + "/")) {
                super.setValue("../" + RESOURCES_FOLDER + getResourceVersion()
                        + this.capturedValue.substring(("../" + RESOURCES_FOLDER).length()));
            }
        }

        return super.doEndTag();
    }

    @Override
    public void setValue(final String value) {
        this.capturedValue = value;
        super.setValue(value);
    }

    /**
     * Gets the resource version.
     *
     * @return the resource version
     */
    private String getResourceVersion() {
        WebApplicationContext webApplicationContext = getRequestContext().getWebApplicationContext();
        VersionProvidingService versionService = webApplicationContext.getBean(VersionProvidingService.class);
        if (versionService == null) {
            throw new NullPointerException("no bean of type `VersionProvidingService` found");
        }
        versionService = webApplicationContext.getBean(VersionProvidingService.class);
        return versionService.getResourceVersion();
    }

}
