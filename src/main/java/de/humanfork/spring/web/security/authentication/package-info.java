/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * Provides an {@link org.springframework.security.web.AuthenticationEntryPoint} that is able to switch between different
 * {@link org.springframework.security.web.AuthenticationEntryPoint}s, depending on the Http Request (Http Response and
 * Authentication Exception).
 *
 * <p>
 * The switch itself is implemented in
 * {@link de.humanfork.spring.web.security.authentication.SwitchingAuthenticationEntryPoint}.
 * This switch needs to be configured with the different switch cases
 * {@link de.humanfork.spring.web.security.authentication.ConditionalAuthenticationEntryPoint}s
 * and a default {@link org.springframework.security.web.AuthenticationEntryPoint}.
 * </p>
 *
 * <p>
 * A typical use case is when an application has an html frontend with some ajax/json requests. Then when the user
 * enter a url that requires a authentication, he should be redirected to the login page
 * ({@link org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint}). But when a AJAX
 * request is hitting an url that requires a authentication (for example because of a session timeout), then
 * the server should return a HTTP status code 403, that is handled by the Java Script on the client side.
 * </p>
 *
 * A typical configuration look like:
 * <pre>
 * {@code
 *    <security:http entry-point-ref="authenticationEntryPoint" ...>
 *    ...
 *    </security:http>
 *
 *    <bean id="loginUrlAuthenticationEntryPoint" class="de.humanfork.spring.web.security.authentication.SwitchingAuthenticationEntryPoint">
 *      <constructor-arg name="defaultAuthenticationEntryPoint">
 *          <bean class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
 *              <constructor-arg name="loginFormUrl" value="/login"/>
 *          </bean>
 *      </constructor-arg>
 *      <property name="conditionalAuthenticationEntryPoints">
 *          <list>
 *              <bean class="de.humanfork.spring.web.security.authentication.ConditionalAuthenticationEntryPointAdapter">
 *                  <constructor-arg name="condition">
 *                      <bean class="org.springframework.security.web.util.matcher.ELRequestMatcher">
 *                          <constructor-arg value="hasHeader('Accept','application/json')"/>
 *                      </bean>
 *                  </constructor-arg>
 *                  <constructor-arg name="authenticationEntryPoint">
 *                      <bean class="org.springframework.security.web.authentication.Http403ForbiddenEntryPoint"/>
 *                  </constructor-arg>
 *              </bean>
 *          </list>
 *      </property>
 *  </bean>
 * }
 * </pre>
 *
 * <p><i>
 * A side note on HTTP status code 403 vs. 401:<br>
 * 403-Forbidden is the correct code, but 401-Unauthorized is not, because:
 * </i></p>
 * <ul>
 *   <li>
 *      401 Unauthorized mean, that the browser should promt a login box and should repeat the request, with respect
 *      to the in the "WWW-Authenticate"-Header-Field defined login mechanism.
 *   </li>
 *   <li>
 *      In contrast a 403 mean that the browser should not AUTOMATICALY do anything.
 *      Btw (The phrase "Authorization will not help" means the HTTP-authorization (in the
 *      "WWW-Authenticate"-Header-Field) will not help. - A 403 response tells the browser that the user does not
 *      have permission to make that request, and the browser should not attempt to collect authentication data and
 *      resubmit the request.
 *   </li>
 * </ul>
 */
package de.humanfork.spring.web.security.authentication;
