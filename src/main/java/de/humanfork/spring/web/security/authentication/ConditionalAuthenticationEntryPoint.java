/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.security.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * An extension of an {@link AuthenticationEntryPoint} with the meaning that this {@link AuthenticationEntryPoint} should
 * only been used when a condition is fulfilled: {@code sConditionFulfilled == true}.
 *
 * @author Ralph Engelmann
 *
 */
public interface ConditionalAuthenticationEntryPoint extends AuthenticationEntryPoint {

    /**
     * Indicate that the condition is fulfilled and therefore this {@link AuthenticationEntryPoint} can been used.
     *
     * @param httpRequest that resulted in an <code>AuthenticationException</code>
     * @param httpResponse so that the user agent can begin authentication
     * @param authException that caused the invocation
     * @return true if this {@link AuthenticationEntryPoint} can been used; else false
     */
    boolean isConditionFulfilled(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
            AuthenticationException authException);
}
