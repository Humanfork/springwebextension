/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.security.authentication;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * A switch for {@link AuthenticationEntryPoint}s.
 * The switch can have several conditional authentication entry points ({@link #conditionalAuthenticationEntryPoints})
 * and (must) have one default entry point ({@link #defaultAuthenticationEntryPoint}).
 *
 * This switch useful for example if one need different authentication entry points for HTML and JSON requests.
 * @author Ralph Engelmann
 */
public class SwitchingAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /**
     * The conditional authentication entry points.
     * Is not null, but can be empty.
     */
    private List<ConditionalAuthenticationEntryPoint> conditionalAuthenticationEntryPoints;

    /**
     * The {@link AuthenticationEntryPoint} that is used if none of the {@link #conditionalAuthenticationEntryPoints}
     * feels responsible.
     * (must be not null)
     */
    private AuthenticationEntryPoint defaultAuthenticationEntryPoint;

    /**
     * Instantiates a new switching authentication entry point.
     *
     * @param defaultAuthenticationEntryPoint the default authentication entry point
     * @param conditionalAuthenticationEntryPoints the conditional authentication entry points
     */
    public SwitchingAuthenticationEntryPoint(final AuthenticationEntryPoint defaultAuthenticationEntryPoint,
            final ConditionalAuthenticationEntryPoint... conditionalAuthenticationEntryPoints) {

        this.defaultAuthenticationEntryPoint = defaultAuthenticationEntryPoint;
        this.conditionalAuthenticationEntryPoints = Arrays.asList(conditionalAuthenticationEntryPoints);
    }

    /**
     * Instantiates a new switching authentication entry point without {@link ConditionalAuthenticationEntryPoint}s.
     *
     * @param defaultAuthenticationEntryPoint the default authentication entry point
     */
    public SwitchingAuthenticationEntryPoint(final AuthenticationEntryPoint defaultAuthenticationEntryPoint) {
        this(defaultAuthenticationEntryPoint, new ConditionalAuthenticationEntryPoint[0]);
    }

    public AuthenticationEntryPoint getDefaultAuthenticationEntryPoint() {
        return this.defaultAuthenticationEntryPoint;
    }

    public List<ConditionalAuthenticationEntryPoint> getConditionalAuthenticationEntryPoints() {
        return this.conditionalAuthenticationEntryPoints;
    }

    public void setConditionalAuthenticationEntryPoints(
            final List<ConditionalAuthenticationEntryPoint> conditionalAuthenticationEntryPoints) {
        this.conditionalAuthenticationEntryPoints = conditionalAuthenticationEntryPoints;
    }

    /**
     * Commence to the (highest ordered) {@link #conditionalAuthenticationEntryPoints} that has a fulfilled condition or
     * use the {@link #defaultAuthenticationEntryPoint} if none of the {@link #conditionalAuthenticationEntryPoints}
     * fulfilled there condition.
     *
     * @param httpRequest that resulted in an <code>AuthenticationException</code>
     * @param httpResponse so that the user agent can begin authentication
     * @param authException that caused the invocation
     */
    @Override
    public void commence(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final AuthenticationException authException)
            throws IOException, ServletException {
        for (ConditionalAuthenticationEntryPoint conditionalEntryPoint : this.conditionalAuthenticationEntryPoints) {
            if (conditionalEntryPoint.isConditionFulfilled(httpRequest, httpResponse, authException)) {
                conditionalEntryPoint.commence(httpRequest, httpResponse, authException);
                return;
            }
        }

        this.defaultAuthenticationEntryPoint.commence(httpRequest, httpResponse, authException);
    }

}
