/*
 * Copyright 2011-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.security.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * A Adapter that adapts an normal {@link AuthenticationEntryPoint} to an  {@link ConditionalAuthenticationEntryPoint}.
 *
 * The condition:
 * {@link ConditionalAuthenticationEntryPoint#isConditionFulfilled(HttpServletRequest, HttpServletResponse, AuthenticationException)}
 * is forward to the {@link #condition} ({@link RequestMatcher}).
 *
 * The authentication entry point:
 * {@link AuthenticationEntryPoint#commence(HttpServletRequest, HttpServletResponse, AuthenticationException)}
 * is forwarded to the {@link #authenticationEntryPoint}.
 */
public class ConditionalAuthenticationEntryPointAdapter implements ConditionalAuthenticationEntryPoint, Ordered {

    /** The condition. */
    private final RequestMatcher condition;

    /** The authentication entry point that can been commenced when the {@link #condition} is true. */
    private final AuthenticationEntryPoint authenticationEntryPoint;

    /** Order according to {@link Ordered#getOrder()}. */
    private int order = -1;

    /**
     * Instantiates a new delegation conditional authentication entry point.
     *
     * @param condition the condition
     * @param authenticationEntryPoint the authentication entry point
     */
    public ConditionalAuthenticationEntryPointAdapter(final RequestMatcher condition,
            final AuthenticationEntryPoint authenticationEntryPoint) {
        this.condition = condition;
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    public RequestMatcher getCondition() {
        return this.condition;
    }

    public AuthenticationEntryPoint getAuthenticationEntryPoint() {
        return this.authenticationEntryPoint;
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    public void setOrder(final int order) {
        this.order = order;
    }

    /* (non-Javadoc)
     * @see com.queomedia.infrastructure.security.authentication.ConditionalAuthenticationEntryPoint#isConditionFulfilled(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
     */
    @Override
    public boolean isConditionFulfilled(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final AuthenticationException authException) {
        return this.condition.matches(httpRequest);
    }

    /* (non-Javadoc)
     * @see org.springframework.security.web.AuthenticationEntryPoint#commence(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
     */
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
            final AuthenticationException authException)
            throws IOException, ServletException {
        this.authenticationEntryPoint.commence(request, response, authException);
    }

}
