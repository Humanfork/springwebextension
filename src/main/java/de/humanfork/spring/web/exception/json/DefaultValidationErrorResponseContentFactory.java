/*
 * Copyright 2011-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.exception.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.metadata.ConstraintDescriptor;

import org.hibernate.validator.engine.HibernateConstraintViolation;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.springframework.context.MessageSource;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

/**
 * Create an response that contains Global Errors and Field Errors.
 *
 * @author Ralph Engelmann
 */
public class DefaultValidationErrorResponseContentFactory implements
        ValidationErrorResponseContentFactory<DefaultValidationErrorResponseContentFactory.ValidationProblem> {

    /** The name of {@link org.hibernate.validator.internal.engine.ConstraintViolationImpl}. */
    static final String HIBERNATE_CONSTRAINT_VIOLATION_IMPLl_CLASSNAME = "org.hibernate.validator.internal.engine.ConstraintViolationImpl";

    /** True if {@link #HIBERNATE_CONSTRAINT_VIOLATION_IMPLl_CLASSNAME} is present. */
    static boolean hibernateConstraintViolationImplPresent = isClassPresent(
            HIBERNATE_CONSTRAINT_VIOLATION_IMPLl_CLASSNAME);

    /**
     * Check if a class with the given {@code className} is present.
     * @param className the full qualified class name
     * @return true if the class is found
     */
    static boolean isClassPresent(final String className) {
        try {
            Class.forName(className, false, DefaultValidationErrorResponseContentFactory.class.getClassLoader());
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    /** The Message Source used to localize the exception messages. */
    private final MessageSource messageSource;

    /**
     * Resolver to convert a Binding-Field-Error path to the field path that is
     * returned to the client.
     */
    private final FieldPathResolver jsonFieldPathResolver;

    /**
     * Feature toggle to enable the  "use default message when not resolvable"-feature.
     * 
     * If the binding-result error (global or field) use a default message key that could not been resolved from the
     * message source, and this feature is enabled, then the {@link ObjectError#getDefaultMessage()} is
     * inserted as message for this key in {@link ErrorDto#getMessages()}.
     */
    private final boolean useDefaultMessageWhenNotResolvable;

    /**
     * Feature toggle to enable the  "respect default message without key"-feature.
     * 
     * If the binding-result error (global or field) contains a default message without key,  
     * then this message would normally ignored, because this resolver assume that the message key is the single source
     * of truth.
     * But if the "respect default message without key"-feature is enabled, and a binding-result error contains a
     * default message without key, then this default message will be returned as default message
     * {@link ErrorDto#defaultMessage} with a {@code ErrorDto.defaultMessageCode = null}. In such a case, the
     * default message will also be added to {@link ErrorDto#getMessages()} and a {@code null} entry will be added to
     * {@link ErrorDto#getMessageCodes()}.
     */
    private final boolean respectDefaultMessageWithoutKey;

    /** Configures if and how the invalid value should been exposed. */
    private final ExposeType exposeInvalidValue;

    /** Configures if and how the constraint annotation arguments should been exposed. */
    private final ExposeType exposeConstraintAnnotationArguments;

    /** Configures if and how the Hibernate Validator parameters annotation arguments should been exposed. */
    private final ExposeType exposeMessageParameters;

    /** Different way of how to expose an information. */
    public static enum ExposeType {
        /** Do not expose. */
        HIDE,

        /** Expose in "arguments" array. */
        ARGUMENTS,

        /** Expose in "messageParameters" array. */
        MESSAGE_PARAMTERS;
    }

    /**
     * Instantiates a new default validation error response content factory.
     *
     * @param messageSource the message source
     * @param jsonFieldPathResolver the json field path resolver
     * @param useDefaultMessageWhenNotResolvable flag that enable the "use default message when not resolvable"-feature - see {@link #useDefaultMessageWhenNotResolvable}.
     * @param respectDefaultMessageWithoutKey flag that enable the "respect default message without key"-feature - see {@link #respectDefaultMessageWithoutKey}.
     * @param exposeInvalidValue if and how the invalid value should been exposed - see {@link #exposeInvalidValue}
     * @param exposeConstraintAnnotationArguments if and how the constraint annotation arguments should been exposed - see {@link #exposeConstraintAnnotationArguments}
     * @param exposeMessageParameters if and how the Hiberernate Validator parameters annotation arguments should been exposed - see {@link #exposeMessageParameters}
     */
    public DefaultValidationErrorResponseContentFactory(final MessageSource messageSource,
            final FieldPathResolver jsonFieldPathResolver,
            final boolean useDefaultMessageWhenNotResolvable,
            final boolean respectDefaultMessageWithoutKey,
            final ExposeType exposeInvalidValue,
            final ExposeType exposeConstraintAnnotationArguments,
            final ExposeType exposeMessageParameters) {
        Objects.requireNonNull(messageSource, "messageSource must not been null");
        Objects.requireNonNull(jsonFieldPathResolver, "jsonFieldPathResolver must not been null");
        Objects.requireNonNull(exposeInvalidValue, "exposeInvalidValue must not been null");
        Objects.requireNonNull(exposeConstraintAnnotationArguments,
                "exposeConstraintAnnotationArguments must not been null");
        Objects.requireNonNull(exposeMessageParameters, "exposeMessageParameters must not been null");

        this.messageSource = messageSource;
        this.jsonFieldPathResolver = jsonFieldPathResolver;
        this.useDefaultMessageWhenNotResolvable = useDefaultMessageWhenNotResolvable;
        this.respectDefaultMessageWithoutKey = respectDefaultMessageWithoutKey;
        this.exposeInvalidValue = exposeInvalidValue;
        this.exposeConstraintAnnotationArguments = exposeConstraintAnnotationArguments;
        this.exposeMessageParameters = exposeMessageParameters;
    }

    /**
     * Instantiates a new default validation error response content factory.
     *
     * @param messageSource the message source
     * @param jsonFieldPathResolver the json field path resolver
     * @param useDefaultMessageWhenNotResolvable flag that enable the "use default message when not resolvable"-feature - see {@link #useDefaultMessageWhenNotResolvable}.
     * @param respectDefaultMessageWithoutKey flag that enable the "respect default message without key"-feature - see {@link #respectDefaultMessageWithoutKey}.
     * @param exposeMessageParameters flag to add the message parameters tin the json response - see {@link #exposeMessageParameters}
     * 
     * @deprecated use {@link DefaultValidationErrorResponseContentFactory#DefaultValidationErrorResponseContentFactory(MessageSource, FieldPathResolver, boolean, boolean, ExposeType, ExposeType, ExposeType)} instead.
     */
    @Deprecated
    public DefaultValidationErrorResponseContentFactory(final MessageSource messageSource,
            final FieldPathResolver jsonFieldPathResolver,
            final boolean useDefaultMessageWhenNotResolvable,
            final boolean respectDefaultMessageWithoutKey,
            final boolean exposeMessageParameters) {
        this(messageSource, jsonFieldPathResolver, useDefaultMessageWhenNotResolvable, respectDefaultMessageWithoutKey,
                ExposeType.ARGUMENTS, ExposeType.ARGUMENTS,
                exposeMessageParameters ? ExposeType.MESSAGE_PARAMTERS : ExposeType.HIDE);
    }

    /**
     * Instantiates a new default validation error response content factory
     * with {@code useDefaultMessageWhenNotResolvable=true}, {@code respectDefaultMessageWithoutKey=true},
     * not exposing the invalid value and exposing constraint annotation arguments and Hibernate message parameters to
     * "messageParameters" array.
     *
     * @param messageSource the message source
     * @param jsonFieldPathResolver the json field path resolver
     */
    public DefaultValidationErrorResponseContentFactory(final MessageSource messageSource,
            final FieldPathResolver jsonFieldPathResolver) {
        this(messageSource, jsonFieldPathResolver, true, true, ExposeType.HIDE, ExposeType.MESSAGE_PARAMTERS,
                ExposeType.MESSAGE_PARAMTERS);
    }

    public MessageSource getMessageSource() {
        return this.messageSource;
    }

    public FieldPathResolver getJsonFieldPathResolver() {
        return this.jsonFieldPathResolver;
    }

    @Override
    public ValidationProblem buildValidationErrorResponseContent(final Locale locale,
            final BindingResult bindingResult) {

        List<ErrorDto> globalErrors = buildGlobalErrorResponse(locale, bindingResult);
        List<FieldErrorDto> fieldErrors = buildFieldErrorResponse(locale, bindingResult);

        return new ValidationProblem(globalErrors, fieldErrors);
    }

    protected List<ErrorDto> buildGlobalErrorResponse(final Locale locale, final BindingResult bindingResult) {
        List<ErrorDto> globalErrors = bindingResult.getGlobalErrors().stream()
                .map(globalError -> toErrorDto(globalError, locale)).collect(Collectors.toList());
        return globalErrors;
    }

    protected List<FieldErrorDto> buildFieldErrorResponse(final Locale locale, final BindingResult bindingResult) {
        List<FieldErrorDto> fieldErrors = bindingResult.getFieldErrors().stream()
                .map(fieldError -> toFieldErrorDto(fieldError, bindingResult, locale)).collect(Collectors.toList());
        return fieldErrors;
    }

    protected static final Set<String> INTERNAL_ANNOTATION_ATTRIBUTES = new HashSet<>(
            Arrays.asList("message", "groups", "payload"));

    private Optional<ConstraintViolation<?>> getConstraintViolation(final ObjectError error) {
        if (error.contains(javax.validation.ConstraintViolation.class)) {
            return Optional.of(error.unwrap(javax.validation.ConstraintViolation.class));
        } else {
            return Optional.empty();
        }
    }

    private Object NO_INVALID_VALUE = new Object();

    private Object getInvalidValue(final ObjectError error) {
        Optional<ConstraintViolation<?>> constraintViolation = getConstraintViolation(error);
        if (constraintViolation.isPresent()) {
            return constraintViolation.get().getInvalidValue();
        } else if (error instanceof FieldError) {
            return ((FieldError) error).getRejectedValue();
        } else {
            return this.NO_INVALID_VALUE;
        }
    }

    protected ErrorDto toErrorDto(final ObjectError error, final Locale locale) {

        Optional<ConstraintViolation<?>> constraintViolation = getConstraintViolation(error);

        final List<String> messageCodes;
        final List<String> messages;

        if (error.getCodes() != null) {
            messageCodes = Stream.of(error.getCodes())
                    .filter(Objects::nonNull)
                    .filter(code -> !code.isEmpty())
                    .collect(Collectors.toList());
            messages = messageCodes.stream()
                    .map(messageCode -> resolveMessage(messageCode, error.getArguments(), locale))
                    .collect(Collectors.toList());
        } else {
            messageCodes = new ArrayList<>();
            messages = new ArrayList<>();
        }

        String defaultMessageCode = null;
        String defaultMessage = null;
        if (constraintViolation.isPresent()) {
            String constraintMessageTemplate = constraintViolation.get().getMessageTemplate();
            if (constraintMessageTemplate != null) {
                String trimed = constraintMessageTemplate.trim();
                if (trimed.startsWith("{") && trimed.endsWith("}")) {
                    defaultMessageCode = trimed.substring(1, trimed.length() - 1);
                }
            }
            defaultMessage = constraintViolation.get().getMessage();

            /* add default message (code) as last element to the list of message (code)s. */
            if ((defaultMessageCode != null) && !messageCodes.contains(defaultMessageCode)) {
                messageCodes.add(defaultMessageCode);
                messages.add(defaultMessage);
            }
        }

        /*
         * only if useDefaultMessageWhenNotResolvable is enabled:
         * 
         * if there is no resolved message for the default error code, but there is a explicit given default message,
         * then assign the default message to the message for the default error code
         */
        if (!StringUtils.isEmpty(error.getCode()) && !StringUtils.isEmpty(error.getDefaultMessage())
                && useDefaultMessageWhenNotResolvable) {

            /*
             * for constraintViolation the default message belongs to constraintViolation.get().getMessageTemplate()
             * else the default message belongs to error.getCode()
             */
            String errorCode;
            if (constraintViolation.isPresent() && !StringUtils.isEmpty(defaultMessageCode)) {
                errorCode = defaultMessageCode;
            } else {
                errorCode = error.getCode();
            }

            int errorCodeIndex = messageCodes.indexOf(errorCode);
            if ((errorCodeIndex >= 0) && (StringUtils.isEmpty(messages.get(errorCodeIndex)))) {
                messages.set(errorCodeIndex, error.getDefaultMessage());
            }
        }

        /* 
         * only if respectDefaultMessageWithoutKey is enabled:
         * if there is a defaultMessage WITHOUT key,
         * then add it to the meassages, and leave the messageCode null 
         */
        if (StringUtils.isEmpty(error.getCode()) && !StringUtils.isEmpty(error.getDefaultMessage())
                && respectDefaultMessageWithoutKey) {
            messageCodes.add(null);
            messages.add(error.getDefaultMessage());
        }

        /* if no default message (code) is set, then the last message from the message (code) list become the default */
        if ((defaultMessageCode == null) && (defaultMessage == null)) {
            if (messageCodes.size() > 0) {
                defaultMessageCode = messageCodes.get(messageCodes.size() - 1);
                defaultMessage = messages.get(messageCodes.size() - 1);
            }
        }

        final List<Argument> arguments = new ArrayList<>();
        final List<Argument> messageParameters = new ArrayList<>();

        Object invalidValue = getInvalidValue(error);
        if (invalidValue != this.NO_INVALID_VALUE) {
            this.expose(new Argument(Argument.FIELD_VALUE_KEY, invalidValue), exposeInvalidValue,
                    arguments,
                    messageParameters);
        }

        if (constraintViolation.isPresent()) {
            this.expose(getArgumentsForConstraint(constraintViolation.get().getConstraintDescriptor()),
                    exposeConstraintAnnotationArguments, arguments, messageParameters);
        } else {
            if (error.getArguments() != null) {
                this.expose(toArguments(error.getArguments()), exposeConstraintAnnotationArguments, arguments,
                        messageParameters);
            }
        }

        if (exposeMessageParameters != ExposeType.HIDE) {
            if (constraintViolation.isPresent() && hibernateConstraintViolationImplPresent) {
                try {
                    HibernateConstraintViolation<?> hibernateConstraintViolation = constraintViolation.get()
                            .unwrap(HibernateConstraintViolation.class);
                    if (hibernateConstraintViolation instanceof ConstraintViolationImpl) {
                        for (Entry<String, Object> messageParamter : ((ConstraintViolationImpl<?>) hibernateConstraintViolation)
                                .getMessageParameters()
                                .entrySet()) {
                            expose(new Argument(messageParamter.getKey(), messageParamter.getValue()),
                                    exposeMessageParameters, arguments, messageParameters);
                        }
                    }
                } catch (ValidationException e) {
                    // the Bean Validation provider implementation does not support the specified class.   
                }
            }
        }

        return new ErrorDto(messageCodes, arguments, messages, defaultMessage, defaultMessageCode, messageParameters);
    }

    /**
     * Assign the values to {@code arguments}, {@code messageParameters}, or none of them - according to {@code target}.
     * @param value the value that is going to be exposed
     * @param target if and how the {@code value} should been exposed
     * @param arguments {@code value} become added to this collection if {@code target} is {@link ExposeType#ARGUMENTS}
     * @param messageParameters {@code value} become added to this collection if {@code target} is {@link ExposeType#ARGUMENTS}
     */
    private void expose(final Collection<Argument> values, final ExposeType target,
            final Collection<Argument> arguments,
            final Collection<Argument> messageParameters) {
        switch (target) {
        case HIDE:
            return;
        case ARGUMENTS:
            arguments.addAll(values);
            return;
        case MESSAGE_PARAMTERS:
            messageParameters.addAll(values);
            return;
        default:
            throw new RuntimeException("case not implemented `" + target + "`");
        }
    }

    private void expose(Argument value, ExposeType target, List<Argument> arguments, List<Argument> messageParameters) {
        this.expose(Arrays.asList(value), target, arguments, messageParameters);
    }

    public static class Argument {
        /** The key of the argument that contains the original field value.*/
        public static final String FIELD_VALUE_KEY = "FIELD_VALUE";

        private final String key;

        private final Object value;

        public Argument(final String key, final Object value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return this.key;
        }

        public Object getValue() {
            return this.value;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = (prime * result) + ((this.key == null) ? 0 : this.key.hashCode());
            result = (prime * result) + ((this.value == null) ? 0 : this.value.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Argument other = (Argument) obj;
            if (this.key == null) {
                if (other.key != null) {
                    return false;
                }
            } else if (!this.key.equals(other.key)) {
                return false;
            }
            if (this.value == null) {
                if (other.value != null) {
                    return false;
                }
            } else if (!this.value.equals(other.value)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "Argument [key=" + this.key + ", value=" + this.value + "]";
        }

    }

    protected Collection<Argument> getArgumentsForConstraint(final ConstraintDescriptor<?> descriptor) {
        Map<String, Argument> attributesToExpose = new TreeMap<>();
        descriptor.getAttributes().forEach((attributeName, attributeValue) -> {
            if (!INTERNAL_ANNOTATION_ATTRIBUTES.contains(attributeName)) {
                attributesToExpose.put(attributeName, new Argument(attributeName, attributeValue));
            }
        });
        return attributesToExpose.values();
    }

    protected List<Argument> toArguments(final Object[] arguments) {
        List<Argument> result = new ArrayList<>();
        for (int i = 0; i < arguments.length; i++) {
            result.add(new Argument(String.valueOf(i), arguments[i]));
        }
        return result;
    }

    protected FieldErrorDto toFieldErrorDto(final FieldError fieldError, final BindingResult bindingResult,
            final Locale locale) {
        String path = this.jsonFieldPathResolver.resolvePath(fieldError, bindingResult);

        return new FieldErrorDto(toErrorDto(fieldError, locale), path, fieldError.isBindingFailure());
    }

    protected String resolveMessage(final String messageCode, final Object[] arguments, final Locale locale) {
        return this.messageSource.getMessage(messageCode, arguments, null, locale);
    }

    /**
     * Response for an Binding Validation error.
     */
    public static class ValidationProblem {

        /** Extension: validation global errors. */
        private final List<ErrorDto> globalErrors;

        /** Extension: validation field errors. */
        private final List<FieldErrorDto> fieldErrors;

        /**
         * Instantiates a new validation problem.
         *
         * @param globalErrors the global errors
         * @param fieldErrors the field errors
         */
        public ValidationProblem(final List<ErrorDto> globalErrors, final List<FieldErrorDto> fieldErrors) {
            this.globalErrors = globalErrors;
            this.fieldErrors = fieldErrors;
        }

        public List<ErrorDto> getGlobalErrors() {
            return this.globalErrors;
        }

        public List<FieldErrorDto> getFieldErrors() {
            return this.fieldErrors;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = (prime * result) + ((this.fieldErrors == null) ? 0 : this.fieldErrors.hashCode());
            result = (prime * result) + ((this.globalErrors == null) ? 0 : this.globalErrors.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ValidationProblem other = (ValidationProblem) obj;
            if (this.fieldErrors == null) {
                if (other.fieldErrors != null) {
                    return false;
                }
            } else if (!this.fieldErrors.equals(other.fieldErrors)) {
                return false;
            }
            if (this.globalErrors == null) {
                if (other.globalErrors != null) {
                    return false;
                }
            } else if (!this.globalErrors.equals(other.globalErrors)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "ValidationProblem [globalErrors=" + this.globalErrors + ", fieldErrors=" + this.fieldErrors + "]";
        }

    }

    public static class ErrorDto {
        private final List<String> messageCodes;

        private final List<Argument> arguments;

        private final List<String> messages;

        private final String defaultMessage;

        private final String defaultMessageCode;

        private final List<Argument> messageParameters;

        public ErrorDto(final List<String> messageCodes, final List<Argument> arguments, final List<String> messages,
                final String defaultMessage, final String defaultMessageCode, final List<Argument> messageParameters) {
            this.messageCodes = messageCodes;
            this.arguments = arguments;
            this.messages = messages;
            this.defaultMessage = defaultMessage;
            this.defaultMessageCode = defaultMessageCode;
            this.messageParameters = messageParameters;
        }

        public List<String> getMessageCodes() {
            return this.messageCodes;
        }

        public List<Argument> getArguments() {
            return this.arguments;
        }

        public List<String> getMessages() {
            return this.messages;
        }

        public String getDefaultMessage() {
            return this.defaultMessage;
        }

        public String getDefaultMessageCode() {
            return this.defaultMessageCode;
        }

        public List<Argument> getMessageParameters() {
            return messageParameters;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((arguments == null) ? 0 : arguments.hashCode());
            result = prime * result + ((defaultMessage == null) ? 0 : defaultMessage.hashCode());
            result = prime * result + ((defaultMessageCode == null) ? 0 : defaultMessageCode.hashCode());
            result = prime * result + ((messageCodes == null) ? 0 : messageCodes.hashCode());
            result = prime * result + ((messageParameters == null) ? 0 : messageParameters.hashCode());
            result = prime * result + ((messages == null) ? 0 : messages.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ErrorDto other = (ErrorDto) obj;
            if (arguments == null) {
                if (other.arguments != null)
                    return false;
            } else if (!arguments.equals(other.arguments))
                return false;
            if (defaultMessage == null) {
                if (other.defaultMessage != null)
                    return false;
            } else if (!defaultMessage.equals(other.defaultMessage))
                return false;
            if (defaultMessageCode == null) {
                if (other.defaultMessageCode != null)
                    return false;
            } else if (!defaultMessageCode.equals(other.defaultMessageCode))
                return false;
            if (messageCodes == null) {
                if (other.messageCodes != null)
                    return false;
            } else if (!messageCodes.equals(other.messageCodes))
                return false;
            if (messageParameters == null) {
                if (other.messageParameters != null)
                    return false;
            } else if (!messageParameters.equals(other.messageParameters))
                return false;
            if (messages == null) {
                if (other.messages != null)
                    return false;
            } else if (!messages.equals(other.messages))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "ErrorDto [messageCodes=" + messageCodes + ", arguments=" + arguments + ", messages=" + messages
                    + ", defaultMessage=" + defaultMessage + ", defaultMessageCode=" + defaultMessageCode
                    + ", messageParameters=" + messageParameters + "]";
        }

    }

    public static class FieldErrorDto extends ErrorDto {

        private final String field;

        private final boolean bindingFailure;

        public FieldErrorDto(final List<String> messageCodes, final List<Argument> arguments,
                final List<String> messages, final String defaultMessage, final String defaultMessageCode,
                final List<Argument> messageParameters, final String field, final boolean bindingFailure) {
            super(messageCodes, arguments, messages, defaultMessage, defaultMessageCode, messageParameters);
            this.field = field;
            this.bindingFailure = bindingFailure;
        }

        public FieldErrorDto(final ErrorDto errorDto, final String field, final boolean bindingFailure) {
            this(errorDto.getMessageCodes(),
                    errorDto.getArguments(),
                    errorDto.getMessages(),
                    errorDto.getDefaultMessage(),
                    errorDto.getDefaultMessageCode(),
                    errorDto.getMessageParameters(),
                    field,
                    bindingFailure);
        }

        public String getField() {
            return this.field;
        }

        public boolean isBindingFailure() {
            return this.bindingFailure;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + (this.bindingFailure ? 1231 : 1237);
            result = (prime * result) + ((this.field == null) ? 0 : this.field.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            FieldErrorDto other = (FieldErrorDto) obj;
            if (this.bindingFailure != other.bindingFailure) {
                return false;
            }
            if (this.field == null) {
                if (other.field != null) {
                    return false;
                }
            } else if (!this.field.equals(other.field)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "FieldErrorDto [field=" + this.field + ", getMessageCodes()=" + getMessageCodes() + ",arguments="
                    + getArguments() + ", messages=" + getMessages() + ", defaultMessage=" + getDefaultMessage()
                    + ", defaultMessageCode=" + getDefaultMessageCode() + ", bindingFailure=" + this.bindingFailure
                    + "]";
        }

    }

}
