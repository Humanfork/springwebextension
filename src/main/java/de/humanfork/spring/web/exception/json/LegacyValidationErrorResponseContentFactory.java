package de.humanfork.spring.web.exception.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class LegacyValidationErrorResponseContentFactory implements
        ValidationErrorResponseContentFactory<LegacyValidationErrorResponseContentFactory.ValidationErrorDto> {

    /** The Message Source used to localize the exception messages. */
    private final MessageSource messageSource;

    private final FieldPathResolver jsonFieldPathResolver;

    public LegacyValidationErrorResponseContentFactory(final MessageSource messageSource,
            final FieldPathResolver jsonFieldPathResolver) {
        if (messageSource == null) {
            throw new IllegalArgumentException("messageSource must not been null");
        }
        if (jsonFieldPathResolver == null) {
            throw new IllegalArgumentException("jsonFieldPathResolver must not been null");
        }

        this.messageSource = messageSource;
        this.jsonFieldPathResolver = jsonFieldPathResolver;
    }

    @Override
    public ValidationErrorDto buildValidationErrorResponseContent(final Locale locale,
            final BindingResult bindingResult) {
        ValidationErrorDto validationErrorDto = new ValidationErrorDto();

        if (bindingResult.hasGlobalErrors()) {
            StringBuilder gobalErrorMessage = new StringBuilder();
            for (Iterator<ObjectError> it = bindingResult.getGlobalErrors().iterator(); it.hasNext();) {
                ObjectError objectError = it.next();
                Message resolvedMessage = resolveMessage(objectError, locale);

                gobalErrorMessage.append(
                        resolvedMessage.getText() != null ? resolvedMessage.getText() : resolvedMessage.getCode());
                validationErrorDto.addGlobalMessage(resolvedMessage);
                if (it.hasNext()) {
                    gobalErrorMessage.append(", ");
                }
            }
            validationErrorDto.setGlobalMessage(gobalErrorMessage.toString());
        }

        for (FieldError fieldError : bindingResult.getFieldErrors()) {

            validationErrorDto.addFieldError(toFieldErrorDto(bindingResult, fieldError, locale));
        }

        return validationErrorDto;
    }

    public FieldErrorDto toFieldErrorDto(final BindingResult bindingResult, final FieldError fieldError,
            final Locale locale) {

        return new FieldErrorDto(this.jsonFieldPathResolver.resolvePath(fieldError, bindingResult),
                resolveMessage(fieldError, locale));
    }

    private Message resolveMessage(final ObjectError fieldError, final Locale locale) {
        try {
            return new Message(fieldError.getCode(), this.messageSource.getMessage(fieldError, locale));
        } catch (NoSuchMessageException e) {
            for (String code : fieldError.getCodes()) {
                return new Message(code, null);
            }
            return null;
        }
    }

    public class ValidationErrorDto {
        @Deprecated
        private String globalMessage;

        private List<Message> globalMessages = new ArrayList<Message>();

        private List<FieldErrorDto> fieldErrorDtos = new ArrayList<FieldErrorDto>();

        public String getGlobalMessage() {
            return this.globalMessage;
        }

        public void setGlobalMessage(final String globalMessage) {
            this.globalMessage = globalMessage;
        }

        public List<Message> getGlobalMessages() {
            return this.globalMessages;
        }

        public void addGlobalMessage(final Message message) {
            this.globalMessages.add(message);
        }

        public void addGlobalMessage(final String messageCode, final String messageText) {
            this.addGlobalMessage(new Message(messageCode, messageText));
        }

        public List<FieldErrorDto> getFieldErrors() {
            return this.fieldErrorDtos;
        }

        public void addFieldError(final FieldErrorDto fieldErrorDto) {
            this.fieldErrorDtos.add(fieldErrorDto);
        }
    }

    public class FieldErrorDto {
        private String field;

        private Message message;

        public FieldErrorDto(final String field, final Message message) {
            super();
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return this.field;
        }

        public String getMessage() {
            return this.message.getText();
        }

        public String getMessageCode() {
            return this.message.getCode();
        }

    }

    public class Message {
        private String code;

        private String text;

        public Message() {
            super();
        }

        public Message(final String code, final String text) {
            this.code = code;
            this.text = text;
        }

        public String getCode() {
            return this.code;
        }

        public void setCode(final String code) {
            this.code = code;
        }

        public String getText() {
            return this.text;
        }

        public void setText(final String text) {
            this.text = text;
        }

    }
}
