/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.exception.json;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * Converter that convert the field path provided in the BindingResult FieldError to the path that is returned in the
 * Field error description of the http response.
 */
public interface FieldPathResolver {

    /**
     * Resolve the response field path for the {@link BindingResult} {@link FieldError} path.
     * @param bindingResultFieldErrorPath the BindingResult FieldError pats
     * @param bindingResultRootClass the binding result root object class
     * @return the field path send to the client
     */
    String resolvePath(String bindingResultFieldErrorPath, Class<? extends Object> bindingResultRootClass);

    /**
     * Resolve the response field path for the {@link BindingResult} {@link FieldError} path.
     *
     * The given {@link FieldError} must be element of the given {@link BindingResult}.
     *
     * @param fieldError the BindingResult FieldError pats
     * @param bindingResult the binding result
     * @return the field path send to the client
     */
    default String resolvePath(final FieldError fieldError, final BindingResult bindingResult) {
        return resolvePath(fieldError.getField(), bindingResult.getTarget().getClass());
    }

}
