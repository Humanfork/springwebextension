/*
 * Copyright 2011-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.exception.json;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

/**
 * A unchecked exception that indicates an failed binding result. This exception
 * is intended to been used for manual rejected Binding Results.
 *
 * Attention: Unecheded Exception will roll back transactions by default.
 *
 * @see #rejectValue(Object, String, String, String)
 * @see ManualBindingResultErrorException for an checked variant
 * @author Engelmann
 */
public class ManualBindingResultErrorRuntimeException extends RuntimeException implements ManualBindingResultError {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8238233547158489947L;

    /** The binding result that express the reason for this exception */
    private final BindingResult bindingResult;

    public ManualBindingResultErrorRuntimeException(final BindingResult bindingResult, final String message,
            final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        if (bindingResult == null) {
            throw new IllegalArgumentException("bindingResult must not been null");
        }

        this.bindingResult = bindingResult;
    }

    public ManualBindingResultErrorRuntimeException(final BindingResult bindingResult, final String message,
            final Throwable cause) {
        super(message, cause);
        if (bindingResult == null) {
            throw new IllegalArgumentException("bindingResult must not been null");
        }

        this.bindingResult = bindingResult;
    }

    public ManualBindingResultErrorRuntimeException(final BindingResult bindingResult, final String message) {
        super(message);
        if (bindingResult == null) {
            throw new IllegalArgumentException("bindingResult must not been null");
        }

        this.bindingResult = bindingResult;
    }

    public ManualBindingResultErrorRuntimeException(final BindingResult bindingResult, final Throwable cause) {
        super(cause);
        if (bindingResult == null) {
            throw new IllegalArgumentException("bindingResult must not been null");
        }

        this.bindingResult = bindingResult;
    }

    public ManualBindingResultErrorRuntimeException(final BindingResult bindingResult) {
        super();
        if (bindingResult == null) {
            throw new IllegalArgumentException("bindingResult must not been null");
        }

        this.bindingResult = bindingResult;
    }

    @Override
    public BindingResult getBindingResult() {
        return this.bindingResult;
    }

    @Override
    public String toString() {
        return super.toString() + " bindingResult.AllErrors: " + this.bindingResult.getAllErrors();
    }

    /**
     * Creates a new instance of the {@link BeanPropertyBindingResult} class with an
     * rejected value.
     *
     * @param target     the target bean to bind onto
     * @param objectName the name of the target object
     * @param field      the field name (may be {@code null} or empty String)
     * @param errorCode  error code, interpretable as a message key
     * @return the manual binding result error runtime exception
     */
    public static ManualBindingResultErrorRuntimeException rejectValue(final Object target, final String objectName,
            final String field, final String errorCode) {
        BeanPropertyBindingResult beanPropertyBindingResult = new BeanPropertyBindingResult(target, objectName);
        beanPropertyBindingResult.rejectValue(field, errorCode);

        return new ManualBindingResultErrorRuntimeException(beanPropertyBindingResult);
    }
}
