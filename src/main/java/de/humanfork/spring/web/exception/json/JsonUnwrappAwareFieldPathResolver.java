/*
 * Copyright 2011-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.exception.json;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Optional;

import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Resolve a field path and try to remove path elements that are annotated with
 * {@link JsonUnwrapped}.
 *
 * <p>
 * This resolver strip all parts of the {@link FieldError#getField()} path that
 * are annotated with {@link JsonUnwrapped}.
 * </p>
 *
 * <p>
 * {@link JsonUnwrapped} is an Jackson/FasterXML annotation that indicate that
 * the content of an Java property should be serialized without the property
 * named contain in JSON. For example:
 * </p>
 * 
 * <pre>
 * <code>
 * public class {
 *    {@literal @}JsonUnwrapped
 *    Container container;
 *
 *    String other;
 * }
 *
 * public class Container {
 *    {@literal @}NotBlank
 *    String content
 * }
 * </code>
 * </pre>
 * 
 * become
 * 
 * <pre>
 * <code>
 * {
 *    "content" : " JsonUnwrapped: is at the same level as other in json"
 *    "other"   : "normal mapped"
 * }
 * </code>
 * </pre>
 *
 * <p>
 * I one want to provide binding results in such an scenario, one do not want to
 * have the Unwrapped Container been part of the client provided path. So in the
 * example about one want to have an Binding error for {@code Container.content}
 * have the just JSON field name "content" but not "container.content". And that
 * is what this Field resolver is for.
 * </p>
 */
public class JsonUnwrappAwareFieldPathResolver implements FieldPathResolver {

    /**
     * If this feature is enabled, then it is expected that {@link Optional} is omitted /fully transparent in the
     * error path.
     * <p>
     * For example, if an {@code classA} contains a field {@code content} of type {@code Optional<String>}, and
     * this feature is enabled, then the expected path is just {@code content}, else the path must be {@code content.get}.  
     * </p>
     */
    private final boolean isOmitOptionalGet;
    
    /**
     * Instantiates a new json unwrapp aware field path resolver.
     *
     * @param omitOptionalGet feature toggle to enable "optional omit get"-Feature - see {@link #isOmitOptionalGet}.
     */
    public JsonUnwrappAwareFieldPathResolver(final boolean omitOptionalGet) {
        this.isOmitOptionalGet = omitOptionalGet;
    }
       
    /**
     * Instantiates a new json unwrapp aware field path resolver with  "optional omit get"-Feature
     * ({@link #isOmitOptionalGet}) enabled.
     */
    public JsonUnwrappAwareFieldPathResolver() {
       this(true);
    }



    @Override
    public String resolvePath(final String bindingResultFieldErrorPath,
            final Class<? extends Object> bindingResultRootClas) {
        return removeWrappFromPath(bindingResultFieldErrorPath, bindingResultRootClas);
    }

    /**
     * Strip all parts of the path that are annotated with {@link JsonUnwrapped}.
     *
     * @param path          the field path
     * @param pathRootClass the root class where the path starts
     * @return the path without fields that are annotated by
     *         {@literal @}{@link JsonUnwrapped}.
     */
    public String removeWrappFromPath(final String path, final Class<? extends Object> pathRootClass) {

        int seperatorIndex = path.indexOf(".");
        if (seperatorIndex < 0) {
            return path;
        }

        String pathSection = path.substring(0, seperatorIndex);

        final String propertyName;
        boolean isCollection;
        if (pathSection.endsWith("[]")) {
            /* set */
            isCollection = true;
            propertyName = pathSection.substring(0, pathSection.length() - 2);
        } else if (pathSection.contains("[") && pathSection.endsWith("]")) {
            /* list */
            isCollection = true;
            propertyName = pathSection.substring(0, pathSection.indexOf("["));
        } else {
            isCollection = false;
            propertyName = pathSection;
        }

        String rest = path.substring(seperatorIndex + 1);

        final boolean isWarpped;
        final Class<?> type;

        Optional<Field> field = getField(pathRootClass, propertyName);
        if (field.isPresent()) {
            isWarpped = field.get().isAnnotationPresent(JsonUnwrapped.class);

            Class<?> fieldType = field.get().getType();
            boolean useGenericType = isCollection || isOptionalType(fieldType);
            if (useGenericType) {
                type = genericTypeResolution(field.get().getGenericType());
            } else {
                type = fieldType;
            }
        } else {
            try {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(propertyName, pathRootClass);
                isWarpped = isGetterOrSetterWrapped(new PropertyDescriptor(propertyName, pathRootClass));

                Class<?> propertyType = propertyDescriptor.getPropertyType();
                boolean useGenericType = isCollection || (this.isOmitOptionalGet && isOptionalType(propertyType));
                if (useGenericType) {
                    type = genericTypeResolution(propertyDescriptor.getReadMethod().getGenericReturnType());
                } else {
                    type = propertyType;
                }
            } catch (IntrospectionException e) {
                throw new RuntimeException(
                        "error while building property descriptor for `" + propertyName + "` on " + pathRootClass,
                        e);
            }
        }

        if (isWarpped) {
            return removeWrappFromPath(rest, type);
        } else {
            return pathSection + "." + removeWrappFromPath(rest, type);
        }
    }

    private boolean isOptionalType(final Class<?> type) {
        return type.equals(Optional.class) || type.getSimpleName().equals("com.google.common.base.Optional");
    }

    @SuppressWarnings("rawtypes")
    protected Class genericTypeResolution(Type genericType) {
        try {

            if (genericType instanceof Class) {
                return (Class) genericType;
            } else if (genericType instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericType;
                parameterizedType.getActualTypeArguments();
                if (parameterizedType.getActualTypeArguments().length != 1) {
                    throw new RuntimeException(
                            "generic type handling for ParameterizedType types currently only supported for types with exactly generic type, but found: "
                                    + "`" + genericType.getTypeName() + "`" + parameterizedType);
                }
                return (Class) parameterizedType.getActualTypeArguments()[0];
            } else if (genericType instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) genericType;
                if (wildcardType.getLowerBounds().length == 1) {
                    return (Class) wildcardType.getLowerBounds()[0];
                } else if (wildcardType.getUpperBounds().length == 1) {
                    return (Class) wildcardType.getLowerBounds()[0];
                } else {
                    throw new RuntimeException(
                            "generic type handling for WildcardType types currently only supported for types with exactly one wildcard: but found: "
                                    + "`" + genericType.getTypeName() + "`" + wildcardType);
                }
            } else {
                throw new RuntimeException("generic type handling for type " + genericType.getTypeName() + " `"
                        + genericType + "` (" + genericType.getClass() + ") is not implemented");
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("generic type handling for type " + genericType.getTypeName() + " `"
                    + genericType + "` (" + genericType.getClass() + ") failed", e);
        }
    }

    protected Optional<Field> getField(final Class<?> clazz, final String name) {
        if (clazz.equals(Object.class)) {
            return Optional.empty();
        }

        for (Field field : clazz.getDeclaredFields()) {
            if (field.getName().equals(name)) {
                return Optional.of(field);
            }
        }

        Class<?> superclass = clazz.getSuperclass();
        if (superclass != null) {
            return getField(superclass, name);
        } else {
            return Optional.empty();
        }
    }

    protected boolean isGetterOrSetterWrapped(final PropertyDescriptor propertyDescriptor) {
        Method readMethod = propertyDescriptor.getReadMethod();
        if ((readMethod != null) && readMethod.isAnnotationPresent(JsonUnwrapped.class)) {
            return true;
        }

        Method writeMethod = propertyDescriptor.getWriteMethod();
        if ((writeMethod != null) && writeMethod.isAnnotationPresent(JsonUnwrapped.class)) {
            return true;
        }

        return false;
    }

}
