/*
 * Copyright 2011-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * Provides an Exception Resolver ( {@link de.humanfork.spring.web.exception.json.JsonValidationExceptionControllerAdvice} )
 * that put the validation error from {@link org.springframework.web.bind.MethodArgumentNotValidException} in the
 * response body.
 *
 * <p>
 * Of course this is only useful for Json Requests. The separation between HTML and Json work by the assumption
 * that HTML Form Submit Handler Methods do not throw an
 * {@link org.springframework.web.bind.MethodArgumentNotValidException} because they take the
 * {@link org.springframework.validation.BindingResult}.
 * </p>
 *
 * <p>
 * The contend that is send to the client in case on such an exception, is provided by the used
 * {@link de.humanfork.spring.web.exception.json.ValidationErrorResponseContentFactory}.
 * BTW: have a look at {@link de.humanfork.spring.web.exception.json.JsonUnwrappAwareFieldPathResolver} if one want to
 * suppress parts of the field path annotated by JsonUnwrap in the response.
 * </p>
 *
 * <p>
 * If one has to do manual field validation in an controller method, then one could throw a
 * {@link org.springframework.web.bind.MethodArgumentNotValidException} in case of an not valid field.
 * But this exception is a checked exception so Spring would not rollback the current transaction!
 * And and it is also cumbersome to create this exception, because it requires a
 * {@link org.springframework.core.MethodParameter} and that requires a {@link java.lang.reflect.Method}
 * object. Therefore we provide two additional exceptions, that are also handled by the
 * {@link de.humanfork.spring.web.exception.json.JsonValidationExceptionControllerAdvice} but are easier to create and
 * come in a checked and in a unchecked fashion.
 * </p>
 * <ul>
 *  <li>{@link ManualBindingResultErrorException}</li>
 *  <li>{@link ManualBindingResultErrorRuntimeException}</li>
 * </ul>
 *
 * This is an usecase example:
 * <pre>
 *  &#64;Transactional
 *  &#64;RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
 *  public User createNewUser(&#64;Valid &#64;RequestBody final UserCreateCommand command) {
 *
 *      if (this.userService.isLoginAlreadyUsed(command.getLogin)) {
 *          throw ManualBindingResultErrorRuntimeException.rejectValue(command,
 *                  "command",
 *                  "login",
 *                  "exists already");
 *      }
 *
 *      return this.userService.createUser(command);
 *  }</pre>
 */
package de.humanfork.spring.web.exception.json;
