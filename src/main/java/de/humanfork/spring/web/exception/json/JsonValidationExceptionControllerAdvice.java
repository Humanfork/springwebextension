/*
 * Copyright 2011-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.web.exception.json;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception Resolver that put the validation error in the response body.
 *
 * This exception resolver will resolve this exceptions:
 * <ul>
 * <li>{@link MethodArgumentNotValidException}</li>
 * <li>{@link ManualBindingResultErrorException}</li>
 * <li>{@link ManualBindingResultErrorRuntimeException}</li>
 * </ul>
 *
 * @author Ralph Engelmann
 */
@ControllerAdvice
public class JsonValidationExceptionControllerAdvice<T> {

    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonValidationExceptionControllerAdvice.class);

    /** The factory used to provide the content that is returned in case of an validation error. */
    private final ValidationErrorResponseContentFactory<T> validationErrorResponseContentFactory;

    /**
     * Constructor .
     * @param validationErrorResponseContentFactory factory to provide content in case of an validation error
     */
    public JsonValidationExceptionControllerAdvice(
            final ValidationErrorResponseContentFactory<T> validationErrorResponseContentFactory) {

        if (validationErrorResponseContentFactory == null) {
            throw new IllegalArgumentException("validationErrorResponseContentFactory must not been null");
        }

        this.validationErrorResponseContentFactory = validationErrorResponseContentFactory;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("JsonValidationExceptionControllerAdvice initialized");
        }
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public T handleMethodArgumentNotValidException(final MethodArgumentNotValidException validationException,
            final Locale locale) {

        return this.validationErrorResponseContentFactory.buildValidationErrorResponseContent(locale,
                validationException.getBindingResult());
    }

    @ExceptionHandler(ManualBindingResultErrorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public T handleManualBindingResultErrorException(final ManualBindingResultErrorException validationException,
            final Locale locale) {

        return this.validationErrorResponseContentFactory.buildValidationErrorResponseContent(locale,
                validationException.getBindingResult());
    }

    @ExceptionHandler(ManualBindingResultErrorRuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public T handleManualBindingResultErrorRuntimeException(
            final ManualBindingResultErrorRuntimeException validationException, final Locale locale) {

        return this.validationErrorResponseContentFactory.buildValidationErrorResponseContent(locale,
                validationException.getBindingResult());
    }
}
