<?xml version="1.0" encoding="UTF-8"?>
<taglib xmlns="http://java.sun.com/xml/ns/j2ee"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
		version="2.0">

	<description>Humanfork Spring Web Extension</description>
	<tlib-version>1.0</tlib-version>
	<short-name>ext</short-name>
	<uri>http://www.humanfork.de/tags/de/humanfork/springwebextension/ext</uri>
	
	<tag>
		<description>URL tag based on the JSTL c:url tag.  This variant is fully 
		backwards compatible with the standard tag.  Enhancements include support 
		for URL template parameters.</description>
		<name>url</name>
		<tag-class>de.humanfork.spring.web.servlet.tags.UrlTag</tag-class>
		<body-content>JSP</body-content>
		<attribute>
			<description>The URL to build.  This value can include template place holders 
			that are replaced with the URL encoded value of the named parameter.  Parameters 
			must be defined using the param tag inside the body of this tag.</description>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Specifies a remote application context path.  The default is the 
			current application context path.</description>
			<name>context</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>The name of the variable to export the URL value to.</description>
			<name>var</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>The scope for the var.  'application', 'session', 'request' and 
			'page' scopes are supported.  Defaults to page scope.  This attribute has no 
			effect unless the var attribute is also defined.</description>
			<name>scope</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Set HTML escaping for this tag, as a boolean value. Overrides the
			default HTML escaping setting for the current page.</description>
			<name>htmlEscape</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Set JavaScript escaping for this tag, as a boolean value.
			Default is false.</description>
			<name>javaScriptEscape</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
</taglib>
