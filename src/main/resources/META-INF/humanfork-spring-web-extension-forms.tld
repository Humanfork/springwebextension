<?xml version="1.0" encoding="UTF-8"?>
<taglib xmlns="http://java.sun.com/xml/ns/j2ee"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
		version="2.0">

	<description>Humanfork Spring Web Extension for forms</description>
	<tlib-version>1.0</tlib-version>
	<short-name>formext</short-name>
	<uri>http://www.humanfork.de/tags/de/humanfork/springwebextension/formext</uri>
	
	<tag>
		<description>Renders a single HTML 'option'. Sets 'selected' if the bound value is null.</description>
		<name>nulloption</name>
		<tag-class>de.humanfork.spring.web.servlet.tags.form.NullOptionTag</tag-class>
		<body-content>JSP</body-content>
		<variable>
			<description>The actual value used for the html 'value' attribute</description>
			<name-given>value</name-given>
			<variable-class>java.lang.Object</variable-class>
		</variable>
		<variable>
			<description>The String representation of thr value bound to the 'value' attribute, taking into consideration
				any PropertyEditor associated with the enclosing 'select' tag.</description>
			<name-given>displayValue</name-given>
			<variable-class>java.lang.String</variable-class>
		</variable>
		<attribute>
			<description>HTML Standard Attribute</description>
			<name>id</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Optional Attribute</description>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Optional Attribute</description>
			<name>label</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Enable/disable HTML escaping of rendered values.</description>
			<name>htmlEscape</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Equivalent to "class" - HTML Optional Attribute</description>
			<name>cssClass</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Equivalent to "class" - HTML Optional Attribute. Used when the bound field has errors.</description>
			<name>cssErrorClass</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>Equivalent to "style" - HTML Optional Attribute</description>
			<name>cssStyle</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Standard Attribute</description>
			<name>lang</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Standard Attribute</description>
			<name>title</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Standard Attribute</description>
			<name>dir</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Standard Attribute</description>
			<name>tabindex</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Optional Attribute. Setting the value of this attribute to 'true' (without the quotes) will disable the HTML element.</description>
			<name>disabled</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onclick</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>ondblclick</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onmousedown</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onmouseup</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onmouseover</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onmousemove</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onmouseout</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onkeypress</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onkeyup</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<description>HTML Event Attribute</description>
			<name>onkeydown</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<dynamic-attributes>true</dynamic-attributes>
	</tag>
</taglib>
