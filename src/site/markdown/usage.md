Usage
=====

The current documentation is within the JavaDoc, mostly at package level.

A util project that contains different small extensions for Spring Web applications.
The main extensions are:

- Exception Resolver that put validation errors in the HTTP-response body (package `de.humanfork.spring.web.exception.json`)
- Spring Security web AuthenticationEntryPoint that is able to switch between different AuthenticationEntryPoints, depending on the HTTP Request (package `de.humanfork.spring.web.security.authentication`)
- Different Servlet Tags (package `de.humanfork.spring.web.servlet.tags`)