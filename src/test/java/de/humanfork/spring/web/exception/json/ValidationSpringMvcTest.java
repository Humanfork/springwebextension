package de.humanfork.spring.web.exception.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import de.humanfork.spring.web.demoapp.DemoController;
import de.humanfork.spring.web.demoapp.DemoController.Demo;
import de.humanfork.spring.web.demoapp.WebConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class ValidationSpringMvcTest {

    @Autowired
    private Validator validator;

    @Test
    public void testPlain() {
        testValidation(Validation.buildDefaultValidatorFactory().getValidator());
    }

    @Test
    public void testSpring() {
        testValidation(this.validator);
    }

    private void testValidation(final Validator validator) {
        Demo demo = new DemoController.Demo("too long text");

        Set<ConstraintViolation<Demo>> violations = validator.validate(demo);

        assertThat(violations).hasSize(1);
        ConstraintViolation<Demo> violation = violations.iterator().next();
        assertThat(violation.getMessageTemplate()).isEqualTo("{javax.validation.constraints.Size.message}");
    }
}
