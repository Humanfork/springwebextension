package de.humanfork.spring.web.exception.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.springframework.context.support.StaticMessageSource;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.ObjectError;

import de.humanfork.spring.web.exception.json.DefaultValidationErrorResponseContentFactory.Argument;
import de.humanfork.spring.web.exception.json.DefaultValidationErrorResponseContentFactory.ExposeType;
import de.humanfork.spring.web.exception.json.DefaultValidationErrorResponseContentFactory.ValidationProblem;

public class DefaultValidationErrorResponseContentFactoryTest {

    public static class Demo {
        public String text;

        public Demo(final String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public void setText(final String text) {
            this.text = text;
        }
    }

    @Test
    public void test() {

        Locale locale = Locale.ENGLISH;
        String messageKey = "someErrorCode";
        String messageTranslation = "some error";

        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.addMessage(messageKey, locale, "some error");

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(locale, bindingResult);

        List<String> expectedErrorCode = Arrays
                .asList(messageKey + ".demo.text", messageKey + ".text", messageKey + ".java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, "some error");

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                Collections.emptyList(),
                expectedMessages,
                messageTranslation,
                messageKey,
                Collections.emptyList(),
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);

    }

    @Test
    public void testDefaultMessage_GlobalError() {
        String defaultMessage = "This is a Default Message";
        String messageKey = "someErrorCode";

        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.addMessage(messageKey, Locale.ENGLISH, defaultMessage);

        ObjectError objectError = new ObjectError("command", new String[] { messageKey }, new Object[] {},
                "This is a Default Message");

        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo("xxx"), "demo");
        bindingResult.addError(objectError);

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                new SimpleFieldPathResolver());

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList(messageKey);
        List<String> expectedMessages = Arrays.asList(defaultMessage);
        List<Argument> expectedArguments = Collections.emptyList();
        DefaultValidationErrorResponseContentFactory.ErrorDto expectedGlobalError = new DefaultValidationErrorResponseContentFactory.ErrorDto(
                expectedErrorCode,
                expectedArguments,
                expectedMessages,
                defaultMessage,
                messageKey,
                Collections.emptyList());
        assertThat(result.getGlobalErrors()).containsExactly(expectedGlobalError);
        assertThat(result.getGlobalErrors().get(0).getDefaultMessage()).isEqualTo(defaultMessage);
        assertThat(result.getGlobalErrors().get(0).getDefaultMessageCode()).isEqualTo(messageKey);

        assertThat(result.getFieldErrors()).isEmpty();
    }

    @Test
    public void testDefaultMessage_GlobalError_withoutMessageKey() {
        String defaultMessage = "This is a Default Message";
        ObjectError objectError = new ObjectError("command", "This is a Default Message");

        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo("xxx"), "demo");
        bindingResult.addError(objectError);

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                new StaticMessageSource(),
                new SimpleFieldPathResolver());

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList((String) null);
        List<String> expectedMessages = Arrays.asList(defaultMessage);
        List<Argument> expectedArguments = Collections.emptyList();
        DefaultValidationErrorResponseContentFactory.ErrorDto expectedGlobalError = new DefaultValidationErrorResponseContentFactory.ErrorDto(
                expectedErrorCode,
                expectedArguments,
                expectedMessages,
                defaultMessage,
                null,
                Collections.emptyList());
        assertThat(result.getGlobalErrors()).containsExactly(expectedGlobalError);
        assertThat(result.getGlobalErrors().get(0).getDefaultMessage()).isEqualTo(defaultMessage);
        assertThat(result.getGlobalErrors().get(0).getDefaultMessageCode()).isNull();

        assertThat(result.getFieldErrors()).isEmpty();
    }

    @Test
    public void testDefaultMessage_FieldlError() {
        String defaultMessage = "default-message-used when key not found";
        String message = "This is a Default Message";
        String messageKey = "someErrorCode";

        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.addMessage(messageKey, Locale.ENGLISH, message);

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey, defaultMessage);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList(messageKey + ".demo.text", messageKey + ".text",
                messageKey + ".java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, message);

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                Collections.emptyList(),
                expectedMessages,
                message,
                messageKey,
                Collections.emptyList(),
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);
    }

    @Test
    public void testDefaultMessage_exposeInvalidValue_argument() {
        String defaultMessage = "default-message-used when key not found";
        String message = "This is a Default Message";
        String messageKey = "someErrorCode";

        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.addMessage(messageKey, Locale.ENGLISH, message);

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver,
                true, true, ExposeType.ARGUMENTS,
                ExposeType.HIDE,
                ExposeType.HIDE);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey, defaultMessage);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList(messageKey + ".demo.text", messageKey + ".text",
                messageKey + ".java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, message);

        List<Argument> expectedArguments = Arrays.asList(new Argument(Argument.FIELD_VALUE_KEY, rejectedValue));

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                expectedArguments,
                expectedMessages,
                message,
                messageKey,
                Collections.emptyList(),
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);
    }

    @Test
    public void testDefaultMessage_exposeInvalidValue_messageParameter() {
        String defaultMessage = "default-message-used when key not found";
        String message = "This is a Default Message";
        String messageKey = "someErrorCode";

        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.addMessage(messageKey, Locale.ENGLISH, message);

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver,
                true, true, ExposeType.MESSAGE_PARAMTERS,
                ExposeType.HIDE,
                ExposeType.HIDE);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey, defaultMessage);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList(messageKey + ".demo.text", messageKey + ".text",
                messageKey + ".java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, message);

        List<Argument> expectedMessageParameters = Arrays.asList(new Argument(Argument.FIELD_VALUE_KEY, rejectedValue));

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                Collections.emptyList(),
                expectedMessages,
                message,
                messageKey,
                expectedMessageParameters,
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);
    }

    @Test
    public void testDefaultMessage_FieldlError_noMessageForKey() {
        String defaultMessage = "default-message-used when key not found";
        String messageKey = "someErrorCode";

        StaticMessageSource messageSource = new StaticMessageSource();

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey, defaultMessage);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList(messageKey + ".demo.text", messageKey + ".text",
                messageKey + ".java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, defaultMessage);

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                Collections.emptyList(),
                expectedMessages,
                defaultMessage,
                messageKey,
                Collections.emptyList(),
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);
    }

    @Test
    public void testDefaultMessage_FieldlError_wihtoutMessageKey() {
        String defaultMessage = "default-message-used when key not found";
        String messageKey = null;

        StaticMessageSource messageSource = new StaticMessageSource();

        SimpleFieldPathResolver jsonFieldPathResolver = new SimpleFieldPathResolver();

        DefaultValidationErrorResponseContentFactory contentFactory = new DefaultValidationErrorResponseContentFactory(
                messageSource,
                jsonFieldPathResolver);

        String rejectedValue = "hallo world";
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(new Demo(rejectedValue), "demo");
        bindingResult.rejectValue("text", messageKey, defaultMessage);

        ValidationProblem result = contentFactory.buildValidationErrorResponseContent(Locale.ENGLISH, bindingResult);

        List<String> expectedErrorCode = Arrays.asList("demo.text", "text", "java.lang.String", messageKey);
        List<String> expectedMessages = Arrays.asList(null, null, null, defaultMessage);

        DefaultValidationErrorResponseContentFactory.FieldErrorDto expectedFieldError = new DefaultValidationErrorResponseContentFactory.FieldErrorDto(
                expectedErrorCode,
                Collections.emptyList(),
                expectedMessages,
                defaultMessage,
                messageKey,
                Collections.emptyList(),
                "text",
                false);
        assertThat(result.getFieldErrors()).containsExactly(expectedFieldError);
    }

    public void testHibernateConstraintViolationImplPresent() {
        assertThat(DefaultValidationErrorResponseContentFactory.hibernateConstraintViolationImplPresent).isTrue();
    }
}
