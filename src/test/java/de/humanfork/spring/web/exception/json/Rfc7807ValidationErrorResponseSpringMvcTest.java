package de.humanfork.spring.web.exception.json;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.humanfork.spring.web.demoapp.DemoController.Demo;
import de.humanfork.spring.web.demoapp.MvcRestDocumentationSetup;
import de.humanfork.spring.web.demoapp.WebConfig;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class Rfc7807ValidationErrorResponseSpringMvcTest {

    private MockMvc mockMvc;

    @Autowired
    private Rfc7807ValidationErrorResponseContentFactory rfc7807Factory;

    /**
     * Register the mvs rest documentation.
     *
     * @param webAppContext the web app context
     * @param restDocumentationContextProvider the rest documentation context provider
     * @see MvcRestDocumentationSetup
     */
    @BeforeEach
    public void setUp(final WebApplicationContext webAppContext,
            final RestDocumentationContextProvider restDocumentationContextProvider) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext)
                .apply(new MvcRestDocumentationSetup(restDocumentationContextProvider)).build();
    }

    @Test
    public void testValidationFailureSize() throws Exception {
        String content = "{'text':'text is too long'}".replace("'", "\"");
        this.mockMvc
                .perform(post("/demo").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("Size.demo.text",
                                "Size.text",
                                "Size.java.lang.String",
                                "Size",
                                "javax.validation.constraints.Size.message"))))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[0].key", Matchers.is("max"))) //alphabetic order, "max" is before "min"
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[0].value", Matchers.is(Demo.TEXT_MAX_LENGTH)))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[1].key", Matchers.is("min")))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[1].value", Matchers.is(Demo.TEXT_MIN_LENGTH)))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage", Matchers.is("size must be between 5 and 10")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode",
                        Matchers.is("javax.validation.constraints.Size.message")))
                .andExpect(jsonPath("$.fieldErrors[0].bindingFailure", Matchers.is(false)));
    }

    @Test
    public void testValidationFailureNotNull() throws Exception {
        String content = "{'text':null}".replace("'", "\"");
        this.mockMvc
                .perform(post("/demo").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("NotNull.demo.text",
                                "NotNull.text",
                                "NotNull.java.lang.String",
                                "NotNull",
                                "javax.validation.constraints.NotNull.message"))))
                .andExpect(jsonPath("$.fieldErrors[0].arguments.*", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage", Matchers.is("must not be null")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode",
                        Matchers.is("javax.validation.constraints.NotNull.message")))
                .andExpect(jsonPath("$.fieldErrors[0].bindingFailure", Matchers.is(false)));
    }

    @Test
    public void testValidationUnwrapp() throws Exception {
        String content = "{'text':null}".replace("'", "\"");
        this.mockMvc
                .perform(post("/jsonUnwrapp").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")));
    }

    @Test
    public void testValidationNotEmpty() throws Exception {
        String content = "{'strings':[]}".replace("'", "\"");
        this.mockMvc
                .perform(post("/notEmpty").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("strings")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("NotEmpty.demoNotEmpty.strings",
                                "NotEmpty.strings",
                                "NotEmpty",
                                "javax.validation.constraints.NotEmpty.message"))))
                .andExpect(jsonPath("$.fieldErrors[0].messages",
                        Matchers.is(Arrays.asList(null, null, null, "must not be empty"))))
                .andExpect(jsonPath("$.fieldErrors[0].arguments.*", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage", Matchers.is("must not be empty")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode",
                        Matchers.is("javax.validation.constraints.NotEmpty.message")));
    }

    @Test
    public void testManualBindingResultError() throws Exception {
        String content = "{'text':'test-123'}".replace("'", "\"");
        this.mockMvc
                .perform(post("/manualBindingResultError").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("manualBindingError.doesNotLikeTheText.demo.text",
                                "manualBindingError.doesNotLikeTheText.text",
                                "manualBindingError.doesNotLikeTheText.java.lang.String",
                                "manualBindingError.doesNotLikeTheText"))))
                .andExpect(jsonPath("$.fieldErrors[0].messages",
                        Matchers.is(Arrays.asList(null, null, null, "The server does not like the entered text"))))
                .andExpect(jsonPath("$.fieldErrors[0].arguments.*", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage",
                        Matchers.is("The server does not like the entered text")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode",
                        Matchers.is("manualBindingError.doesNotLikeTheText")));
    }
    
    @Test
    public void testFieldErrorWithoutKey() throws Exception {
        String content = "{'text':'test-123'}".replace("'", "\"");
        this.mockMvc
                .perform(post("/fieldErrorWithoutKey").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(print())
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.title", Matchers.is(this.rfc7807Factory.getTitleDefaultMessage())))
                .andExpect(jsonPath("$.titleMessageCode", Matchers.is(this.rfc7807Factory.getTitleMessageCode())))
                .andExpect(jsonPath("$.globalErrors", Matchers.empty()))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("demo.text",
                                "text",
                                "java.lang.String",
                                null))))
                .andExpect(jsonPath("$.fieldErrors[0].messages",
                        Matchers.is(Arrays.asList(null, null, null, "because there is no message key in some cases"))))
                .andExpect(jsonPath("$.fieldErrors[0].arguments.*", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage",
                        Matchers.is("because there is no message key in some cases")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode", Matchers.emptyOrNullString()));
    }

}
