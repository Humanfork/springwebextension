package de.humanfork.spring.web.exception.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class JsonUnwrappAwareFieldPathResolverTest {

    static class Demo {
        private String text;
    }

    static class DemoWrapper {
        @JsonUnwrapped
        private Demo demo;
    }

    static class DemoContainer {
        private Demo demo;
    }
    
    static class DemoContainerGetter {
        public Demo getDemo() {return null;}
        public void setDemo(Demo demo) {}
    }

    static class DemoSet {
        private Set<Demo> demos;
    }

    static class DemoContainerSet {
        private Set<DemoContainer> containers;
    }
    
    static class DemoContainerOptional {
        private Optional<DemoContainer> demoContainer;
    }
    
    static class DemoContainerOptionalGetter {
        public Optional<DemoContainer> getDemoContainer() {return null;}
        public void setDemoContainer(Optional<DemoContainer> demoContainer) {}
    }
    
    static class DemoContainerSetGetter {
        public Set<DemoContainer> getContainers() {return null;}
        public void setContainers(Set<DemoContainer> containers) {}
    }

    static class DemoWrapperSet {
        private Set<DemoWrapper> demoWrappers;
    }
    
    
    static class DemoList {
        private List<Demo> demos;
    }

    static class DemoContainerList {
        private List<DemoContainer> containers;
    }
    
    static class DemoContainerListGetter {
        public List<DemoContainer> getContainers() {return null;}
        public void setContainers(List<DemoContainer> containers) {}
    }

    static class DemoWrapperList {
        private List<DemoWrapper> demoWrappers;
    }
    
    static class DemoArray {
        private Demo[] demos;
    }


    @Test
    public void testResolvePath_Demo() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("text", Demo.class)).isEqualTo("text");
    }

    @Test
    public void DemoWrapper() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demo.text", DemoWrapper.class))
                .isEqualTo("text");
    }

    @Test
    public void testResolvePath_DemoSet() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demos[].text", DemoSet.class))
                .isEqualTo("demos[].text");
    }

    @Test
    public void testResolvePath_DemoContainer() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demo.text", DemoContainer.class))
                .isEqualTo("demo.text");
    }

    @Test
    public void testResolvePath_DemoContainerSet() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("containers[].demo.text", DemoContainerSet.class))
                        .isEqualTo("containers[].demo.text");
    }

    @Test
    public void testResolvePath_DemoWrapperSet() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demoWrappers[].demo.text", DemoWrapperSet.class))
                        .isEqualTo("demoWrappers[].text");
    }
    
    @Test
    public void testResolvePath_DemoOptional() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demoContainer.demo.text", DemoContainerOptional.class))
                        .isEqualTo("demoContainer.demo.text");
    }
    
    @Test
    public void testResolvePath_DemoOptionalGetter() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demoContainer.demo.text", DemoContainerOptionalGetter.class))
                        .isEqualTo("demoContainer.demo.text");
    }
    
    @Test
    public void testResolvePath_DemoContainerGetter() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demo.text", DemoContainerGetter.class))
                .isEqualTo("demo.text");
    }

    @Test
    public void testResolvePath_DemoContainerSetGetter() {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("containers[].demo.text", DemoContainerSetGetter.class))
                        .isEqualTo("containers[].demo.text");
    }
    
    
    
    
    
    @Test
    public void testResolvePath_DemoList() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demos[0].text", DemoList.class))
                .isEqualTo("demos[0].text");
    }

    @Test
    public void testResolvePath_DemoContainerList() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("containers[0].demo.text", DemoContainerList.class))
                        .isEqualTo("containers[0].demo.text");
    }

    @Test
    public void testResolvePath_DemoWrapperList() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demoWrappers[0].demo.text", DemoWrapperList.class))
                        .isEqualTo("demoWrappers[0].text");
    }
   

    @Test
    public void testResolvePath_DemoContainerListGetter() {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(
                jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("containers[0].demo.text", DemoContainerListGetter.class))
                        .isEqualTo("containers[0].demo.text");
    }
    
    
    @Test
    public void testResolvePath_DemoArray() throws Exception {
        JsonUnwrappAwareFieldPathResolver jsonUnwrappAwareFieldPathResolver = new JsonUnwrappAwareFieldPathResolver();

        assertThat(jsonUnwrappAwareFieldPathResolver.removeWrappFromPath("demos[0].text", DemoArray.class))
                .isEqualTo("demos[0].text");
    }
}
