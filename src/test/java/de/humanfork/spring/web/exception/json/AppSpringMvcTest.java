package de.humanfork.spring.web.exception.json;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.humanfork.spring.web.demoapp.WebConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class AppSpringMvcTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MessageSource messageSource;

    private MockMvc mockMvc;

    /**
     * Register the mvs rest documentation.
     *
     * @param webAppContext the new up
     * @see de.humanfork.spring.web.demoapp.MvcRestDocumentationSetup
     */
    @BeforeEach
    public void setUp(final WebApplicationContext webAppContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void test() {
        assertThat(this.applicationContext).isNotNull();
    }

    @Test
    public void testController() throws Exception {
        String content = "{'text':'hello'}".replace("'", "\"");
        this.mockMvc.perform(post("/demo").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                .content(content)).andExpect(status().isOk());
    }

    @Test
    @Disabled
    public void validationMessages() throws Exception {
        String message = this.messageSource
                .getMessage("javax.validation.constraints.Size.message", new Object[0], null, Locale.ENGLISH);
        assertThat(message).isNotNull();
    }

}
