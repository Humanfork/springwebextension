package de.humanfork.spring.web.exception.json;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.humanfork.spring.web.demoapp.WebConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class JsonUnwrappAwareFieldPathResolverSpringMvcTest {

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(final WebApplicationContext webAppContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void testValidationFailureSize() throws Exception {
        String content = "{'demos':[{'text':'text is too long'}]}".replace("'", "\"");
        this.mockMvc
                .perform(post("/demoSet").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andDo(print()).andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("demos[].text")))
                .andExpect(jsonPath("$.fieldErrors[0].messageCodes",
                        Matchers.is(Arrays.asList("Size.demoSet.demos[].text",
                                "Size.demoSet.demos.text",
                                "Size.demos[].text",
                                "Size.demos.text",
                                "Size.text",
                                "Size",
                                "javax.validation.constraints.Size.message"))));
    }
}
