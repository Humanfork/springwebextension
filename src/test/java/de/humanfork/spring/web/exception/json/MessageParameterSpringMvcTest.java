package de.humanfork.spring.web.exception.json;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.humanfork.spring.web.demoapp.MessageParameterDemoConstraintValidator;
import de.humanfork.spring.web.demoapp.WebConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class MessageParameterSpringMvcTest {
    
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(final WebApplicationContext webAppContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void testCustomValidationFailureSize() throws Exception {
        String content = "{'text':'demo text'}".replace("'", "\"");
        this.mockMvc
                .perform(post("/messageParameterDemo/custom").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode", Matchers.is("de.humanfork.spring.web.demoapp.MessageParameterDemoConstraintValidator.message")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage", Matchers.is("this messag use a parameter `the value` for demonstration")))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[0].key", Matchers.is(MessageParameterDemoConstraintValidator.MY_MESSAGE_PARAMETER)))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[0].value", Matchers.is("the value")));
    }
    
    @Test
    public void testDefaultValidationFailureSize() throws Exception {
        String content = "{'text':'to long demo text'}".replace("'", "\"");
        this.mockMvc
                .perform(post("/messageParameterDemo/default").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", Matchers.is(400)))
                .andExpect(jsonPath("$.fieldErrors[0].field", Matchers.is("text")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessageCode", Matchers.is("org.hibernate.validator.constraints.Length.message")))
                .andExpect(jsonPath("$.fieldErrors[0].defaultMessage", Matchers.is("length must be between 3 and 10")))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters.*", Matchers.hasSize(2)))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[?(@.key=='min')].value", Matchers.contains(3)))
                .andExpect(jsonPath("$.fieldErrors[0].messageParameters[?(@.key=='max')].value", Matchers.contains(10)));
    }
}
