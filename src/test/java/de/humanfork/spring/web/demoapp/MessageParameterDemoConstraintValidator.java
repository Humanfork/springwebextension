package de.humanfork.spring.web.demoapp;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

/**
 * Custom JSR 303 validator to demonstrate message parameter handling.
 * 
 * This validator will ALWAYS report a validation failuere.
 * Its failure message will have a parameter: {@link #MY_MESSAGE_PARAMETER}.
 * 
 * @author engelmann
 */
public class MessageParameterDemoConstraintValidator implements ConstraintValidator<MessageParameterDemoConstraintValidator.MessageParameteDemoCustomConstraint, Object> {
    
    @Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = MessageParameterDemoConstraintValidator.class)
    public @interface MessageParameteDemoCustomConstraint {
        
        String message() default "{de.humanfork.spring.web.demoapp.MessageParameterDemoConstraintValidator.message}";
        
        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    public static final String MY_MESSAGE_PARAMETER = "myMessageParameter";
    
    private MessageParameteDemoCustomConstraint constraintAnnotation;

    @Override
    public void initialize(final MessageParameteDemoCustomConstraint constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation; 
    }
    
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        
        HibernateConstraintValidatorContext hibernateContext = context
                .unwrap(HibernateConstraintValidatorContext.class);

        hibernateContext.disableDefaultConstraintViolation();

        hibernateContext.addMessageParameter(MY_MESSAGE_PARAMETER, "the value");
        hibernateContext
                .buildConstraintViolationWithTemplate(constraintAnnotation.message())
                .addBeanNode()        
                .addConstraintViolation();

        return false;
    }
}
