package de.humanfork.spring.web.demoapp;

import java.util.Arrays;
import java.util.List;

import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.config.SnippetConfigurer;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcConfigurer;

/**
 * Configure the {@link MockMvcBuilder} to create Rest Doc via {@link MockMvcRestDocumentation}.
 *
 * Using this class:
 * <pre><code>
 * {@literal @}BeforeEach
 * public void setUp(WebApplicationContext webAppContext, RestDocumentationContextProvider restDocContextProvider) {
 *     this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext)
 *             .apply(new MvcRestDocumentationSetup(restDocContextProvider))
 *             .build();
 * }
 * </code></pre>
 * is equal to:
 * <pre><code>
 * {@literal @}BeforeEach
 * public void setUp(WebApplicationContext webAppContext, RestDocumentationContextProvider restDocContextProvider) {
 *     this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext)
 *             .apply(MockMvcRestDocumentation.documentationConfiguration(restDocContextProvider))
 *             .alwaysDo(MockMvcRestDocumentation.document("{method-name}",
 *                     Preprocessors.preprocessRequest(Preprocessors.prettyPrint()),
 *                     Preprocessors.preprocessResponse(Preprocessors.prettyPrint())));
 *             .build();
 * }
 * </code></pre>
 *
 * <p>
 * Attention: if one add <code>.andDo(document("name",..))</code> to an test method,
 * then his create an <b>additional</b> "operation", but does not extend or override the "default" one defined
 * in this setup.
 * </p>
 */
public class MvcRestDocumentationSetup implements MockMvcConfigurer {

    private RestDocumentationContextProvider restDocumentationContextProvider;

    public MvcRestDocumentationSetup(final RestDocumentationContextProvider restDocumentationContextProvider) {
        this.restDocumentationContextProvider = restDocumentationContextProvider;
    }

    @Override
    public void afterConfigurerAdded(final ConfigurableMockMvcBuilder<?> builder) {
        internalSetupRestDocumentation(builder);
    }

    public static <T extends ConfigurableMockMvcBuilder<T>> T setupRestDocumentation(final T mockMvcBuilder,
            final RestDocumentationContextProvider restDocContextProvider) {
        MvcRestDocumentationSetup setup = new MvcRestDocumentationSetup(restDocContextProvider);
        setup.internalSetupRestDocumentation(mockMvcBuilder);
        return mockMvcBuilder;
    }

    /**
     * See https://docs.spring.io/spring-restdocs/docs/2.0.3.RELEASE/reference/html5/#documentating-your-api-parameterized-output-directories
     * for directory name place holder
     *
     * @param mockMvcBuilder the mock mvc builder
     */
    public void internalSetupRestDocumentation(final ConfigurableMockMvcBuilder<?> mockMvcBuilder) {
        // @formatter:off
        mockMvcBuilder
                .apply(
                        MockMvcRestDocumentation.documentationConfiguration(this.restDocumentationContextProvider)
                            .uris()
                                .withScheme("http")
                                .withPort(80)
                                .withHost("demo.com")
                                .and()
                            .snippets()
                                .withDefaults(defaultSnippets().toArray(new Snippet[0]))
                         )
                .alwaysDo(allwaysDoRestDocumentationResultHandler());
        // @formatter:on
    }

    /**
     * Override this method if you want other Snippets than the defaults from {@link SnippetConfigurer} (defaultSnippets).
     *
     * @return the snippet list
     */
    public List<Snippet> defaultSnippets() {
        return Arrays.asList(CliDocumentation.curlRequest(),
                CliDocumentation.httpieRequest(),
                HttpDocumentation.httpRequest(),
                HttpDocumentation.httpResponse(),
                PayloadDocumentation.requestBody(),
                PayloadDocumentation.responseBody());
    }

    public RestDocumentationResultHandler allwaysDoRestDocumentationResultHandler() {
        return MockMvcRestDocumentation.document("{class-name}_{methodName}",
                Preprocessors.preprocessRequest(Preprocessors.prettyPrint()),
                Preprocessors.preprocessResponse(Preprocessors.prettyPrint()));
    }
}
