package de.humanfork.spring.web.demoapp;

import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import de.humanfork.spring.web.exception.json.DefaultValidationErrorResponseContentFactory.ValidationProblem;
import de.humanfork.spring.web.exception.json.FieldPathResolver;
import de.humanfork.spring.web.exception.json.JsonUnwrappAwareFieldPathResolver;
import de.humanfork.spring.web.exception.json.JsonValidationExceptionControllerAdvice;
import de.humanfork.spring.web.exception.json.Rfc7807ValidationErrorResponseContentFactory;

@ComponentScan(basePackageClasses = WebConfig.class)
@EnableWebMvc
@Configuration //(proxyBeanMethods = true)
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.addBasenames("classpath:messages");
        messageSource.addBasenames("classpath:org.hibernate.validator.ValidationMessages");
        return messageSource;
    }

    @Bean
    public Rfc7807ValidationErrorResponseContentFactory responseContentFactory(final MessageSource messageSource) {
        FieldPathResolver fieldPathResolver = new JsonUnwrappAwareFieldPathResolver();
        return new Rfc7807ValidationErrorResponseContentFactory(messageSource, fieldPathResolver);
    }

    @Bean
    public JsonValidationExceptionControllerAdvice<ValidationProblem> jsonValidationExceptionControllerAdvice(
            final Rfc7807ValidationErrorResponseContentFactory responseContentFactory) {
        return new JsonValidationExceptionControllerAdvice<>(responseContentFactory);
    }

    private Jackson2ObjectMapperBuilder jacksonBuilder() {
        return new Jackson2ObjectMapperBuilder().indentOutput(true);
    }

    @Override
    public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
        converters.add(new MappingJackson2HttpMessageConverter(jacksonBuilder().build()));
    }

    @Override
    public org.springframework.validation.Validator getValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource());
        return validator;
    }

}
