package de.humanfork.spring.web.demoapp;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.humanfork.spring.web.demoapp.MessageParameterDemoConstraintValidator.MessageParameteDemoCustomConstraint;

@RestController
@RequestMapping(path = "/messageParameterDemo")
public class MessageParameterDemoController {

    public static class CustomMessageParameterDemoDto {
        
        @MessageParameteDemoCustomConstraint        
        private String text;

        public CustomMessageParameterDemoDto() {
            super();
        }

        public String getText() {
            return this.text;
        }

        public void setText(final String text) {
            this.text = text;
        }
    }
    
    
    public static class DefaultMessageParameterDemoDto {
        
        @Length(min = 3, max = 10)        
        private String text;

        public DefaultMessageParameterDemoDto() {
            super();
        }

        public String getText() {
            return this.text;
        }

        public void setText(final String text) {
            this.text = text;
        }
    }
    
    @PostMapping(path = "/custom")
    public void checkCustomDemo(@RequestBody @Valid final CustomMessageParameterDemoDto messageParameterDemoDto) {
        System.out.println(messageParameterDemoDto);
    }
    
    @PostMapping(path = "/default")
    public void checkDefaultDemo(@RequestBody @Valid final DefaultMessageParameterDemoDto messageParameterDemoDto) {
        System.out.println(messageParameterDemoDto);
    }
    
}
