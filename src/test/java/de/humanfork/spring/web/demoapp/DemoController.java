package de.humanfork.spring.web.demoapp;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import de.humanfork.spring.web.exception.json.ManualBindingResultErrorRuntimeException;

@RestController
@RequestMapping
public class DemoController {

    public static class Demo {

        public static final int TEXT_MIN_LENGTH = 5;

        public static final int TEXT_MAX_LENGTH = 10;

        @Size(min = TEXT_MIN_LENGTH, max = TEXT_MAX_LENGTH)
        @NotNull
        private String text;

        public Demo() {
            super();
        }

        public Demo(@Size(min = 5, max = 10) @NotNull final String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public void setText(final String text) {
            this.text = text;
        }
    }

    public static class DemoSet {
        private Set<@Valid Demo> demos;

        public Set<Demo> getDemos() {
            return demos;
        }

        public void setDemos(Set<Demo> demos) {
            this.demos = demos;
        }
    }

    public static class DemoWrapper {

        @JsonUnwrapped
        @Valid
        private Demo demo;

        public Demo getDemo() {
            return this.demo;
        }

        public void setDemo(final Demo demo) {
            this.demo = demo;
        }

    }

    public static class DemoNotEmpty {

        @NotEmpty
        public Set<String> strings;
    }

    @PostMapping(path = "/demo")
    public void checkDemo(@RequestBody @Valid final Demo demo) {
        System.out.println(demo);
    }

    @PostMapping(path = "/demoSet")
    public void checkDemoSet(@RequestBody @Valid final DemoSet demoSet) {
        System.out.println(demoSet);
    }

    @PostMapping(path = "/jsonUnwrapp")
    public void jsonUnwrapp(@RequestBody @Valid final DemoWrapper demoWrapper) {
        System.out.println(demoWrapper);
    }

    @PostMapping(path = "/notEmpty")
    public void jsonUnwrapp(@RequestBody @Valid final DemoNotEmpty demoNotEmpty) {
        System.out.println(demoNotEmpty);
    }

    @PostMapping(path = "/manualBindingResultError")
    public void manualBindingResultError(@RequestBody final Demo demo) {
        throw ManualBindingResultErrorRuntimeException
                .rejectValue(demo, "demo", "text", "manualBindingError.doesNotLikeTheText");
    }
    
    @PostMapping(path = "/fieldErrorWithoutKey")
    public void fieldErrorWithoutKey(@RequestBody final Demo demo) {
                
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(demo, "demo");       
        String defaultMessage = "because there is no message key in some cases";       
        String messageKey = null;
        bindingResult.rejectValue("text", messageKey, defaultMessage);
        
        throw new ManualBindingResultErrorRuntimeException(bindingResult);
    }
}
